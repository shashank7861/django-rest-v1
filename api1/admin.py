from django.contrib import admin
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from .models import Post, UserProfile

admin.site.register(Post)
admin.site.register(UserProfile)
